library(tibble)
library(ggplot2)
library(paletteer)

set.seed(4)


# df <- lseries(sample(seq(0, 50, 0.0001), size = 1))
# df$color <- c(rep("A", 125), rep("B", 250), rep("C", 250), rep("D", 250), rep("A",125))
# df 
# wespal <- wesanderson::wes_palette("Darjeeling1", n = 4)
# ggplot(df)+
#   geom_path(aes(x= a, y, color = color), lineend = "butt", linejoin = "bevel")+
#   coord_polar()+
#   theme_void()+
#   theme(legend.position = "none", 
#         panel.background = element_rect(fill = "#000000",
#                                         colour = "#000000",
#                                         size = 0.5, linetype = "solid"))+
#   scale_color_manual(values = paletteer::paletteer_d(palette = "wesanderson::Darjeeling1", n = 4))+
#   ggsave("1_opt.png")
# 
# df %>% write.csv("1_q32.csv")

########################
gen_data_lambert <- function(lambert_f = sin, qval = NULL, min_q = 0, max_q = 50) {
  if (is.null(qval)) {
     qval <- sample(seq(min_q, max_q, 0.0001), size = 1)
     print(paste0("q is: ", qval))
  }
  x <- 1:1000
  fx <- sapply(x, lambert_f)
  x1x <-  x^qval / (1 - x^qval)
  y <- fx * x1x
  color <- c(rep("A", 125), rep("B", 250), rep("C", 250), rep("D", 250), rep("A",125))
  
  return(tibble(a = x, y = y, fx = fx, x1x = x1x, qval = qval, color = color))
}

plot_lambert <- function(df, pal = paletteer::paletteer_d(palette = "wesanderson::Darjeeling1", n = 4)) {
  ggplot(df)+
    geom_path(aes(x= a, y, color = color), lineend = "butt", linejoin = "bevel")+
    coord_polar()+
    theme_void()+
    theme(legend.position = "none", 
          panel.background = element_rect(fill = "#000000",
                                          colour = "#000000",
                                          size = 0.5, linetype = "solid"))+
    scale_color_manual(values = pal)
}

for (i in 0:20) {
  df <- gen_data_lambert(qval = i)
  p <- plot_lambert(df, pal_dar)
  p + ggsave(paste0("out/01_lambert_series/sin/q_", i, ".png"))
}

for (i in 0:20) {
  df <- gen_data_lambert(qval = i, lambert_f = function(x) sin(2 * x) * 3)
  p <- plot_lambert(df, pal_dar)
  p + ggsave(paste0("out/01_lambert_series/arctan_q_", i, ".png"))
}

plot_lambert_w <- function(df) {
  ggplot(df)+
    geom_path(aes(x= a, y), color = "white", lineend = "butt", linejoin = "bevel")+
    coord_polar()+
    theme_void()+
    theme(legend.position = "none", 
          panel.background = element_rect(fill = "#000000",
                                          colour = "#000000",
                                          size = 0.5, linetype = "solid"))
}
plot_lambert(gen_data_lambert(qval = 110, lambert_f = cos))
df <- gen_data_lambert(qval = 110)
df
